import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'record-user';
  @ViewChild('videoElement')
  videoElement!: ElementRef;

  @ViewChild('buttonRecord', { read: ElementRef, static: false })
  buttonRecord!: ElementRef;



  frames: string[] = [];

  constructor(private el: ElementRef) {
    
  }
  

  ngOnInit() {
    navigator.mediaDevices.getUserMedia({ video: true }).then((stream) => {
      const videoElement = this.videoElement.nativeElement;
      videoElement.srcObject = stream;
      videoElement.play();
    });
  }

  startRecording() {
    this.frames = [];
    const videoElement = this.videoElement.nativeElement;
    const buttonRecord = this.buttonRecord.nativeElement;
    const canvasElement = <HTMLCanvasElement>document.createElement('canvas');
    canvasElement.width = videoElement.videoWidth;
    canvasElement.height = videoElement.videoHeight;
    const context = canvasElement.getContext('2d');

    if(this.buttonRecord.nativeElement.classList.contains('notRec')){
      this.buttonRecord.nativeElement.classList.remove('notRec');
      this.buttonRecord.nativeElement.classList.add('Rec');
    }else{
      this.buttonRecord.nativeElement.classList.remove('Rec');
      this.buttonRecord.nativeElement.classList.add('notRec');
    }
  
    

    let i = 0;
    const intervalId = setInterval(() => {
      if (context != null) {
        context.drawImage(
          videoElement,
          0,
          0,
          canvasElement.width,
          canvasElement.height
        );
      }

      this.frames.push(canvasElement.toDataURL('image/jpeg'));
      i++;
      if (i >= 5) {
        this.buttonRecord.nativeElement.classList.remove('Rec');
        this.buttonRecord.nativeElement.classList.add('notRec');
        clearInterval(intervalId);
      }
    }, 1000);
  }
}
